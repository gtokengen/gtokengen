const createAndSignJwt = (_config) => new Promise((resolve,reject)=>{
  const config = _config || {};
  const creds = config.creds || "";
  const privateKey = creds.private_key || "";
  
  const rs = require('jsrsasign');

  const timespan = (time, iat) => {
      const ms = require('ms');
      
      var timestamp = iat || Math.floor(Date.now() / 1000);
  
      if (typeof time === 'string') {
        var milliseconds = ms(time);
        if (typeof milliseconds === 'undefined') {
          return;
        }
        return Math.floor(timestamp + milliseconds / 1000);
      } else if (typeof time === 'number') {
        return timestamp + time;
      } else {
        return;
      }    
  }
  const iat = Math.floor(Date.now() / 1000);
  const exp = timespan('1h',iat);

  const payload = {
      iss:creds.client_email || "",
      iat:iat,
      exp:exp,
      aud:"https://oauth2.googleapis.com/token",
      scope:"" // most important part, you add many scopes here.. dunno if it's comma seperated
  }

  const scopes = creds.scope || "";
  if(scopes.length > 0){
      const scopeArray = scopes.split("/n");
      scopeArray.forEach((scope)=>{
          payload.scope += ` ${scope}`;
      })
  }

  
  const jwt = rs.KJUR.jws.JWS.sign("RS256", {alg: "RS256"}, payload, privateKey);

  return resolve(jwt)
  
  
});
const getGoogleToken = (_config) => new Promise(async (resolve,reject) => {
  const config = _config || {};

  const _ = require('lodash');
  if(_.isEmpty(config)) return resolve({token:"",errors:[{"title":"NO_CONFIG_PROVIDED"}]})

  const creds = _config.creds || "";
  if(_.isEmpty(creds)) return resolve({token:"",errors:[{"title":"NO_CREDS_PROVIDED"}]})

  const jwt = await createAndSignJwt({creds});

  const bent = require('bent');
  const post = bent('https://oauth2.googleapis.com/', 'POST', 'json', 200);
  const data = {
      grant_type:"urn:ietf:params:oauth:grant-type:jwt-bearer",
      assertion:jwt
  }

  const response = await post('token',data);
  const token = response.access_token;
  
  
  return resolve({token});
})

const tokenGen = {
    async hello(config){
        try{
            const token = await getGoogleToken(config);
            
           return token;
        }catch(err){
            console.log({err});
        }
    }
}

export default tokenGen;