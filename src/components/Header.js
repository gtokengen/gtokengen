export function Header() {
    return (
      <div className="header_section">
        <div className="header">
          <a className="logo center_text" href="/">gtokengen</a>
          <a className="nav center_text" href="/Overview">
            Docs
          </a>
          <div className=" center_text">
            <span className="built_by">Built by</span> Sam & Muna
          </div>
        </div>
      </div>
    );
  }