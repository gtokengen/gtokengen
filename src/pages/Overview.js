import { Header } from "../components/Header";
import "../styles/app.scss"

import React from "react";

export function Overview() {
  return (
    <>
     <Header/>
      <div className="docs_body">
        <div className="docs_section">
          <div className="side_nav">
            <div className="nav_section">
              <div className="nav_content">
                <a className="nav_item" href="/Overview">
                  Overview
                </a>
                <br />
                <a className="nav_item" href="/Docs">
                  OAuth 2.0 Scopes
                </a>
              </div>
            </div>
          </div>
          <div className="overview">
            <div className="scopes_page_title">Overview</div>
            <div className="overview_content">
              Google APIs use the OAuth 2.0 protocol for authentication and
              authorization. Google supports common OAuth 2.0 scenarios such as
              those for web server, client-side, installed, and limited-input
              device applications. To begin, obtain OAuth 2.0 client credentials
              from the Google API Console. Then your client application requests
              an access token from the Google Authorization Server, extracts a
              token from the response, and sends the token to the Google API
              that you want to access. For an interactive demonstration of using
              OAuth 2.0 with Google (including the option to use your own client
              credentials), experiment with the OAuth 2.0 Playground.
              <br />
              All applications follow a basic pattern when accessing a Google
              API using OAuth 2.0. At a high level, you follow five steps:
              <div className="step">
                1. Obtain OAuth 2.0 credentials from the Google API Console.
              </div>
              Visit the{" "}
              <a
                href="https://console.cloud.google.com/apis/"
                className="overview_link"
              >
                Google API Console
              </a>{" "}
              to obtain OAuth 2.0 credentials such as a client ID and client
              secret that are known to both Google and your application. The set
              of values varies based on what type of application you are
              building. For example, a JavaScript application does not require a
              secret, but a web server application does. You must create an
              OAuth client appropriate for the platform on which your app will
              run, for example:
              <ul className="overview_list">
                <li>
                  For server-side or JavaScript web apps use the "web" client
                  type. Do not use this client type for any other application,
                  such as native or mobile apps.
                </li>
                <li> For Android apps, use the "Android" client type.</li>
                <li>For iOS and macOS apps, use the "iOS" client type.</li>
                <li>
                  {" "}
                  For Universal Windows Platform apps, use the "Universal
                  Windows Platform" client type.
                </li>
                <li>
                  For limited input devices, such as TV or embedded devices, use
                  the "TVs and Limited Input devices" client type.
                </li>
                <li>
                  {" "}
                  For{" "}
                  <a
                    className="overview_link"
                    href="https://developers.google.com/identity/protocols/oauth2/service-account#creatinganaccount"
                  >
                    server-to-server interactions
                  </a>
                  , use service accounts.
                </li>
              </ul>
              <div className="step">
                2. Obtain an access token from the Google Authorization Server.
              </div>
              Before your application can access private data using a Google
              API, it must obtain an access token that grants access to that
              API. A single access token can grant varying degrees of access to
              multiple APIs. A variable parameter called scope controls the set
              of resources and operations that an access token permits. During
              the access-token request, your application sends one or more
              values in the scope parameter. There are several ways to make this
              request, and they vary based on the type of application you are
              building. For example, a JavaScript application might request an
              access token using a browser redirect to Google, while an
              application installed on a device that has no browser uses web
              service requests. Some requests require an authentication step
              where the user logs in with their Google account. After logging
              in, the user is asked whether they are willing to grant one or
              more permissions that your application is requesting. This process
              is called user consent. If the user grants at least one
              permission, the Google Authorization Server sends your application
              an access token (or an authorization code that your application
              can use to obtain an access token) and a list of scopes of access
              granted by that token. If the user does not grant the permission,
              the server returns an error. It is generally a best practice to
              request scopes incrementally, at the time access is required,
              rather than up front. For example, an app that wants to support
              saving an event to a calendar should not request Google Calendar
              access until the user presses the "Add to Calendar" button: see
              <a
                className="overview_link"
                href="https://developers.google.com/identity/protocols/oauth2/web-server#incrementalAuth"
              >
                <span /> Incremental authorization
              </a>
              .
              <div className="step">
                3. Examine scopes of access granted by the user.
              </div>
              Compare the scopes included in the access token response to the
              scopes required to access features and functionality of your
              application dependent upon access to a related Google API. Disable
              any features of your app unable to function without access to the
              related API. The scope included in your request may not match the
              scope included in your response, even if the user granted all
              requested scopes. Refer to the documentation for each Google API
              for the scopes required for access. An API may map multiple scope
              string values to a single scope of access, returning the same
              scope string for all values allowed in the request. Example: the
              Google People API may return a scope of
              https://www.googleapis.com/auth/contacts when an app requested a
              user authorize a scope of https://www.google.com/m8/feeds/; the
              Google People API method people.updateContact requires a granted
              scope of https://www.googleapis.com/auth/contacts.
              <div className="step">4. Send the access token to an API.</div>
              After an application obtains an access token, it sends the token
              to a Google API in an HTTP Authorization request header. It is
              possible to send tokens as URI query-string parameters, but we
              don't recommend it, because URI parameters can end up in log files
              that are not completely secure. Also, it is good REST practice to
              avoid creating unnecessary URI parameter names. Access tokens are
              valid only for the set of operations and resources described in
              the scope of the token request. For example, if an access token is
              issued for the Google Calendar API, it does not grant access to
              the Google Contacts API. You can, however, send that access token
              to the Google Calendar API multiple times for similar operations.
              <div className="step">
                {" "}
                5. Refresh the access token, if necessary
              </div>
              Access tokens have limited lifetimes. If your application needs
              access to a Google API beyond the lifetime of a single access
              token, it can obtain a refresh token. A refresh token allows your
              application to obtain new access tokens
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
