
import "../styles/app.scss"
import React, { useState, useEffect } from "react";
import { Header } from "../components/Header";

export function Docs() {
  const [apis, setApis] = useState([]);

  useEffect(() => {
    fetch("https://apis.gtokengen.com/google.json")
      .then((response) => response.json())
      .then((data) => {
        setApis(data);
      });
  }, []);

  return (
    <>
    <Header/>
      <div className="docs_body">
        <div className="docs_section">
          <div className="side_nav">
            <div className="nav_section">
              <div className="nav_content">
                <a className="nav_item" href="/Overview">Overview</a>
                <div className="nav_item" href="/Docs">OAuth 2.0 Scopes</div>
              </div>
            </div>
          </div>
          <div className="scopes">
            {" "}
            <div className="scopes_page_title">Google Auth API Scopes</div>
            <div className="scopes_section">
              {apis.map((api) => (
                <div key={api.name}>
                  <h2 className="scope_name">
                    <a href={api.url}>{api.name}</a>
                  </h2>
                  <div>
                    <table className="scopes_table">
                      <thead>
                        <tr>
                          <th colSpan="2">Scopes</th>
                        </tr>
                      </thead>
                      <tbody>
                        {api.scopes.map((scope, index) => (
                          <tr key={index}>
                            <td>{scope.scope}</td>
                            <td>{scope.description}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
