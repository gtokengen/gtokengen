import { createRoot } from "react-dom/client";
import { App } from "./App";
import React from "react";
import { Docs } from "./pages/Docs";
import { Overview } from "./pages/Overview";
// import './index.css';
import "./styles/app.scss"
import {
    createBrowserRouter,
    RouterProvider,
} from "react-router-dom";


const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
  },
  {
    path: "/Docs",
    element: <Docs />,
  },
  {
    path: "/Overview",
    element: <Overview />,
  },
]);


const container = document.getElementById("app");
const root = createRoot(container)
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
